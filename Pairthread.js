import { self } from 'react-native-threads';
import RNFetchBlob from 'rn-fetch-blob'
import AsyncStorage from '@react-native-community/async-storage';
import {DateFormats} from './components/utility'

self.onmessage = async(message) => {
    
    if(message=='Start_Getlogue'){
        self.postMessage('Start_Getlogue');
        await AsyncStorage.setItem('dogetlogue','go');
        var devid=await AsyncStorage.getItem('devid')
        getalive(devid,true)
    }
    if(message=='Stop_Getlogue'){
        self.postMessage('Stop_Getlogue');
        await AsyncStorage.setItem('dogetlogue','');
    }
    if(message=='Start'){
        self.postMessage('Start');
        await AsyncStorage.setItem('dopair','go');
        getdevid_length()
    }
    if(message=='Stop'){
        self.postMessage('Stop');
        await AsyncStorage.setItem('dopair','');
    }
}
getalive=async(devid,first)=>{
    self.postMessage('進入getalive')
    try{
        let body = JSON.stringify({
            "act": "getAlive",
                "devIds": [devid]            
        })
        RNFetchBlob.config({}).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type':'application/json'},body)
        .then(async(res)=>{
            self.postMessage(res.text())
            if (res.json().arr[devid]==null){
                /*self.postMessage('devid 10分鐘內未傳送訊息')
                if(first){
                    var isfirst = await AsyncStorage.getItem(devid)
                    if(first == null){

                    }else{

                    }
                }else{
                    setTimeout(async() => {
                        if(await AsyncStorage.getItem('dogetlogue')=='go') getalive(devid,false)
                    },3000)
                }*/
                //var first = await AsyncStorage.getItem(devid)                

            }else{
                self.postMessage('此id 非null')

            }
            
        }).catch((err)=>{
            self.postMessage("getlogue 解析錯誤1")
        })
    }catch(err){
        self.postMessage("getlogue 解析錯誤2")
    }
}
getdevid_length=async()=>{
    self.postMessage('進入getdevid');
    var token=await AsyncStorage.getItem('token')
    await AsyncStorage.removeItem('pair_length')
    try{
        let body = JSON.stringify({})
        RNFetchBlob.config({
        }).fetch('GET','https://dimension8ai.com/auth/api/accountLinks/me/mac-address',
        {'Content-Type': 'application/json','Authorization':'Bearer '+token},body)
        .then(async(res)=>{
            pair_complete(Object.keys(res.json()).length,token)
        }).catch((err) => {
            self.postMessage("getdevid_length 解析錯誤1")
        })
    }catch(err){
        self.postMessage("getdevid_length 解析錯誤2")
    }
}

pair_complete=async(length,token)=>{
    self.postMessage('比較length');
    try{
        let body = JSON.stringify({})
        RNFetchBlob.config({
        }).fetch('GET','https://dimension8ai.com/auth/api/accountLinks/me/mac-address',
        {'Content-Type': 'application/json','Authorization':'Bearer '+token},body)
        .then(async(res)=>{
            if(length==Object.keys(res.json()).length){
                self.postMessage('配對未完成');
                setTimeout(async() => {
                    if(await AsyncStorage.getItem('dopair')=='go') pair_complete(length,token)
                },500)
            }else{
                var devid = res.json()[Object.keys(res.json()).length-1].provider_user_id
                self.postMessage('偵測到新devid:'+devid);
                report_devid(devid)
            }
        } ).catch((err) => {
            self.postMessage('pair_complete 解析錯誤1')
          })
    }catch(err){
        self.postMessage('pair_complete 解析錯誤2')
    }
}

report_devid=async(devid)=>{
    self.postMessage('開始report_devid')
    const project = devid.substring(0,devid.indexOf('-'))
    const username = await AsyncStorage.getItem('username');
    var time = DateFormats(new Date())
    try{
        let body = JSON.stringify({
            "act": "saveDevId",
            "fields": {
                "devId": devid
            },
            "newValues": {
                "devId": devid,
                "userId": username,
                "platform": "D8LINK",
                "project": project,
                "registerTime": time,
                "comment": "",
                "devAppVersion": "1.1.0.00"
            }
        })
        self.postMessage(body)
        RNFetchBlob.config({
        }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
        .then((res)=>{
            //self.postMessage('report_devid'+res.text())
            self.postMessage('report_devid 回傳成功:\n'+res.text())
            creatSip(devid,time)
        } ).catch((err) => {
            self.postMessage('report_devid 解析錯誤1')
          })
    }catch(err){
        self.postMessage('report_devid 解析錯誤2')
    }
}
creatSip=async(devid,time)=>{
    self.postMessage('開始creatSip')
    try{
        let body = JSON.stringify({
            "act": "saveSip",
            "fields": {
                "devId": devid
            },
            "newValues": {
                "devId": devid,
                "updateTime": time,
                "app": {
                    "speaker": {},
                    "autoanswer": false,
                    "microphone": {},
                    "company": "d8ai",
                    "sipua": {
                        "ua": [
                            {
                                "address": "125.227.53.126",
                                "domain": "125.227.53.126",
                                "username": "3110",
                                "plainpassword": "abc123",
                                "port": 5060,
                                "protocol": "udp"
                            }
                        ],
                        "config": {
                            "enable_mwi": true,
                            "outbound": {
                                "timeout": 20
                            }
                        }
                    }
                },
                "contacts": []
            }
        })
        RNFetchBlob.config({
        }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
        .then((res)=>{
            self.postMessage('creatSip 回傳成功:\n'+res.text())
            self.postMessage('配對完成');
        } ).catch((err) => {
            console.log("解析錯誤")
          })
    }catch(err){
    }
}



