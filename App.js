/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  StatusBar,
  Button,
  NativeModules,
  TouchableOpacity,
  Image
} from 'react-native';
const {Smart}=NativeModules;
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {AsyncStorage} from 'react-native';
import Login_sc from './components/Login_sc';
import Register_sc from './components/Register_sc';
import Choose_sc from './components/Choose_sc';
import Main_sc from './components/Main_sc';
import Pair_sc from './components/Pair_sc';
import Setting_sc from './components/Setting_sc';
import Connect_sc from './components/Connect_sc';
import Dispair_sc from './components/Dispair_sc';
import Sip_sc from './components/Sip_sc';
import Sipedit_sc from './components/Sipedit_sc';
import Dialogue_sc from './components/Dialogue_sc';
class HomeScreen extends React.Component {
  static navigationOptions={
    header:null
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Button
          title="Go to Details... again"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </View>
    );
  }
}
const RooStack = createStackNavigator(
  {
    Login:Login_sc,
    Register:Register_sc,
    Choose:Choose_sc,
    Main:Main_sc,
    Dialogue:Dialogue_sc,
    Pair:Pair_sc,
    Setting:Setting_sc,
    Sip:Sip_sc,
    Sipedit:Sipedit_sc,
    Dispair:Dispair_sc,
    Connect:Connect_sc,
  }
);
const AppContainer =createAppContainer(RooStack);


export default class App extends React.Component{
  render(){
    return(
      <Dialogue_sc/>
    )
  }
}
