module.exports = {
    dependencies: {
        'react-native-threads': {
            platforms: {
                android: null,
            },
        },
        '@react-native-community/async-storage': {
            platforms: {
                android: null,
            },
        },
        'react-native-gesture-handler': {
            platforms: {
                android: null,
            },
        },
        'react-native-vector-icons': {
            platforms: {
                android: null,
            },
        },
        'rn-fetch-blob': {
            platforms: {
                android: null,
            },
        },
    },
}