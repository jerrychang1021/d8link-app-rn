package com.d8ai_rn;

import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Map;
import java.util.HashMap;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class Volley1Module extends ReactContextBaseJavaModule {
  /*private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";*/
  private RequestQueue requestQueue_login;
  private JSONObject output_login;
  private int DEFAULT_SLEEP_MS = 60000;
  private int DEFAULT_CLOUD_TIMEOUT_MS = 5000;
  private int DEFAULT_LOG_QUEUE_MAX = 256;
  private Boolean suc_login;
  private String userID,username,token;


  public Volley1Module(ReactApplicationContext reactContext) {
      super(reactContext);
  }
  @Override
  public String getName() {
      return "Volley1";
  }
  /*@Override
   public Map<String, Object> getConstants() {
     final Map<String, Object> constants = new HashMap<>();
     constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
     constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
     Log.v("runnning","Map_OK");
     return constants;
   }*/
  @ReactMethod
  public void login(String account,String password) {
      suc_login =  false;
      requestQueue_login = Volley.newRequestQueue(getReactApplicationContext());
      Toast.makeText(getReactApplicationContext(), "login收到", Toast.LENGTH_SHORT).show();
      output_login = new JSONObject();
      try {
          output_login.put("auth_type","basic");
          output_login.put("username",account);
          output_login.put("password",password);
          //send_login(output_login);
      }catch (JSONException e){e.printStackTrace();}
      JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,"https://dimension8ai.com/auth/api/sessions", output_login, new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
              String[] keys = {"user_id","user_name","id","token","error"};
              for(String key : keys){
                  switch (key){
                      case ("user_id"):
                          try {
                              Log.v("runnning",response.toString(0));
                              suc_login=true;
                              userID = response.getString(key);
                          }catch (JSONException e){e.printStackTrace();}
                          break;
                      case("user_name"):
                          try {
                              if (response.getString(key) != null){
                                  username = response.getString(key);
                              }
                          }catch (JSONException e) {e.printStackTrace();}
                          break;
                      case("token"):
                          try {
                              if (response.getString(key) != null){
                                  token = response.getString(key);
                              }
                          }catch (JSONException e) {e.printStackTrace();}
                          break;
                      case("error"):
                          try {
                              if (response.getString(key) != null)
                                  suc_login = false;
                          }catch (JSONException e) {e.printStackTrace();}
                          break;
                  }
              }
              if(suc_login) {
                  Toast.makeText(getReactApplicationContext(), "登入成功", Toast.LENGTH_SHORT).show();
                  Log.v("runnning","登入成功");
              } else {
                  Toast.makeText(getReactApplicationContext(), "登入失敗! 請確認帳號與密碼!", Toast.LENGTH_SHORT).show();
                  Log.v("runnning","登入失敗");
              }
          }
      },new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
              Log.v("runnning","404");
              Toast.makeText(getReactApplicationContext(), "登入失敗", Toast.LENGTH_SHORT).show();
          }
      });
      jsObjRequest.setShouldCache(false);
      jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
              DEFAULT_CLOUD_TIMEOUT_MS,
              DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
              DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
      requestQueue_login.add(jsObjRequest);
  }

}

