package com.d8ai_rn;

import android.util.Log;
import android.widget.Toast;

import com.broadcom.neeze.Neeze;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;


public class SmartModule extends ReactContextBaseJavaModule {

  /*private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";*/

  public SmartModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }
  @Override
  public String getName() {
    return "Smart";
  }
  /*@Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();
    constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
    constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
    Log.v("runnning","Map_OK");
    return constants;
  }*/

  /*@ReactMethod
  public void SmartConfig(String message) {
    Neeze.send("D8AI", "d8ai$$3012", 0, 1729);
  }*/
  Thread mThread = null;
  private Boolean mDone=false;
  @ReactMethod
  public void SmartConfig(final String wifi,final String password) {
    Log.v("runnning","送出:"+"\n"+"wifi:"+wifi+"\n"+"password:"+password);
    if (!mDone) {
      mDone = true;
      if (mThread == null) {
        mThread = new Thread() {
          public void run() {
            while (mDone) {
              Log.v("runnning","正在 SmartCongig");
              Neeze.send(wifi, password, 0, 1729);
              if (!mDone){
                mThread = null;
                break;
              }
            }
          }
        };
      }
      mThread.start();
    }
  }
  @ReactMethod
  public void StopConfig() {
    mDone=false;
  }
  @ReactMethod
  public void Show(String message) {
    Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_SHORT).show();
  }
  @ReactMethod
  public void Log(String message) {
    Log.v("runnning",message);
  }
}