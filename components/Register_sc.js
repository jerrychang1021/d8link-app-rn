import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler
} from 'react-native';
const {Smart}=NativeModules;
import { CheckBox } from 'react-native-elements'
import RNFetchBlob from 'rn-fetch-blob'
import AsyncStorage from '@react-native-community/async-storage';
export default class Register_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this.send = this.send.bind(this)
        this.gotologin = this.gotologin.bind(this)                
        this.state={
            text_usn:'',
            text_act:'',
            text_psw:'',
            text_psw_ag:'',
            checked:false
        }
    }
    componentDidMount(){
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    goback=async()=>{
        this.props.navigation.replace('Login')
    }
    post(message){
        Smart.Log(message)
        console.log(message)
    }
    send(){
        emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/
        if (!this.state.checked)Smart.Show('尚未同意使用者條款')
        else if(this.state.text_usn=='')Smart.Show('姓名欄位不可為空')
        else if(this.state.text_act=='')Smart.Show('帳號欄位不可為空')
        else if(this.state.text_psw=='')Smart.Show('密碼欄位不可為空')
        else if(this.state.text_psw_ag=='')Smart.Show('確認密碼欄位不可為空')
        else if(this.state.text_usn.match(' ')||this.state.text_act.match(' ')||this.state.text_psw.match(' ')||this.state.text_psw_ag.match(' '))Smart.Show('欄位不可有空白字符')
        else if(this.state.text_psw!=this.state.text_psw_ag)Smart.Show('密碼不一致，請重新確認')
        else if(this.state.text_act.search(emailRule) == -1)Smart.Show('E-mail格式不對')
        else{
            try{
                let body = JSON.stringify({
                    name:this.state.text_usn,
                    privilege:'user',
                    accountLinks:[
                        {
                            auth_type: "basic",
                            username: this.state.text_act,
                            password: this.state.text_psw
                        }
                    ]
                })
                this.post(body)
                RNFetchBlob.config({
                }).fetch('POST','https://dimension8ai.com/auth/api/users',{'Content-Type': 'application/json'},body)
                .then((res)=>{
                    this.post(res.text())
                    if(res.json().id != null){
                        this.post("註冊成功")
                        Smart.Show('註冊成功')
                        this._storeData(this.state.text_act,this.state.text_psw)
                    }
                } ).catch((err) => {
                    Smart.Show('註冊失敗，帳號已註冊')
                    this.post("註冊失敗，帳號已註冊")
                  })
            }catch(err){
                this.post('未知錯誤')
            }
        }
    }

    _storeData = async (account,password) => {
        try{
            await AsyncStorage.setItem('account',account);
            await AsyncStorage.setItem('password',password);
        }catch (error) {
            this.post('登入儲存錯誤')
        }
        this.gotologin()
    }
    gotologin(){
        this.props.navigation.navigate('Login')
    }

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}>
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}}onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>註冊帳號</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2',flexDirection:'row'}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:39}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>姓名</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(text_usn)=>this.setState({text_usn})}
                            placeholder='請輸入'></TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>帳號</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(text_act)=>this.setState({text_act})}
                            placeholder='請輸入'></TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>密碼</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(text_psw)=>this.setState({text_psw})}
                            placeholder='請輸入'
                            secureTextEntry={true}></TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>確認密碼</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(text_psw_ag)=>this.setState({text_psw_ag})}
                            placeholder='請輸入'
                            secureTextEntry={true}></TextInput>
                        </View>
                        <View style={{flex:29}}></View>
                        <View style={{height:24,flexDirection:'row'}}>
                            <CheckBox
                            checked={this.state.checked} 
                            onPress={() => this.setState({ checked: !this.state.checked })}>
                            </CheckBox>
                            <Text style={{fontSize:16,paddingTop:4}}>我已同意</Text>
                            <Text style={{fontSize:16,paddingTop:4,color:'#4a90e2'}}onPress={()=>{Smart.Show('尚未載入使用者條款')}}>使用者條款</Text>
                        </View>
                        <View style={{flex:28}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity style={styles.button1} onPress={this.send}>
                                <Text style={styles.buttontext1}>送出</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:81}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button1:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
    buttontext1:{
        flex:1,
        fontSize:14,
        textAlign:"center",
        textAlignVertical: 'center',
        color:"#ffffff"
    },
    textInput1:{
        flex:1,
        backgroundColor:"#ffffff",
        paddingLeft:15,
        textAlignVertical:'center',
        borderRadius:22,
        borderColor:"#000000",
        borderWidth:1,
        fontSize:16,
    }
});