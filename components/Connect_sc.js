import React from 'react'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TextInput,
    StatusBar,
    Button,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler
} from 'react-native';
const {Smart}=NativeModules;
export default class Connect_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)        
        this.btn_net = this.btn_net.bind(this)   
        this.doconnect = this.doconnect.bind(this)                               
        this.state={
            width1:1,
            height1:1,
            wifi:'',
            password:'',
            btn_color:'#ff6800',
            btn_text:'開始'
        }
    }
    btn_press=false
    componentDidMount(){
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        Smart.StopConfig()
        this.backHandler.remove();
    }
    goback=async()=>{
        Smart.StopConfig()
        this.props.navigation.navigate('Setting')
    }
    btn_net(){
        if(this.state.wifi=='')Smart.Show('WIFI欄位不可為空')
        else if(this.state.password=='')Smart.Show('密碼欄位不可為空')
        else if(this.state.password.length<8)Smart.Show('密碼字數須為8位以上')
        else if(this.btn_press){
            this.setState({
                btn_color:'#ff6800',
                btn_text:'開始'
            });
            Smart.StopConfig()
            this.btn_press=!this.btn_press
        }else{
            this.setState({
                btn_color:'#ff0000',
                btn_text:' 停止'
            },()=>{});
            Smart.SmartConfig(this.state.wifi,this.state.password)
            this.btn_press=!this.btn_press
        }
    }
    doconnect(){
        if(this.btn_press){
            console.log('連線')
        }
        setTimeout(this.doconnect(),1000)
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}>
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}}onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>音箱配網</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2',flexDirection:'row'}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:39}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>wifi</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(wifi)=>this.setState({wifi})}
                            placeholder='請輸入'></TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>密碼</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(password)=>this.setState({password})}
                            placeholder='請輸入'
                            secureTextEntry={true}></TextInput>
                        </View>
                        <View style={{flex:31}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity 
                            style={{
                                flex:1,
                                backgroundColor:this.state.btn_color,
                                color:"#ffffff",
                                fontSize:14,
                                borderRadius:22,}}
                            onPress={this.btn_net}>
                                <Text style={styles.buttontext1}>{this.state.btn_text}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:311}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button1:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
    buttontext1:{
        flex:1,
        fontSize:14,
        textAlign:"center",
        textAlignVertical: 'center',
        color:"#ffffff"
    },
    textInput1:{
        flex:1,
        backgroundColor:"#ffffff",
        paddingLeft:15,
        textAlignVertical:'center',
        borderRadius:22,
        borderColor:"#000000",
        borderWidth:1,
        fontSize:16,
    }
});