import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    NativeModules,
    TouchableOpacity,
    Image,
    Picker,
    Platform,
    BackHandler
} from 'react-native';
const {Smart}=NativeModules;
import RNFetchBlob from 'rn-fetch-blob'
import AsyncStorage from '@react-native-community/async-storage';
import {DateFormats} from './utility'
export default class Dispair_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this._onLayout1 = this._onLayout1.bind(this)  
        this._onValueChange = this._onValueChange.bind(this)
        this.dispair = this.dispair.bind(this)
        this.state={
            width1:1,
            height1:1,
            dropdown:'',
            devids:''
        }
        this.getdevid()
    }
    componentDidMount(){
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    goback=async()=>{
        this.props.navigation.navigate('Setting')
    }
    post(message){
        Smart.Log(message)
        console.log(message)
    }
    getdevid=async()=>{
        this.devids=[]
        this.token=await AsyncStorage.getItem('token')
        try{
            let body = JSON.stringify({})
            RNFetchBlob.config({
            }).fetch('GET','https://dimension8ai.com/auth/api/accountLinks/me/mac-address',
            {'Content-Type': 'application/json','Authorization':'Bearer '+this.token},body)
            .then((res)=>{
                this.post(res.text())
                if(Object.keys(res.json()).length==0) this.devids.push({label: '尚未配對任何機型',value:0})

                else for(i=0;i<Object.keys(res.json()).length;i++){
                    this.devids.push({label: res.json()[i].provider_user_id,value:i})
                }
                this.setState({
                    devids:''
                },()=>{});
            } ).catch((err) => {
                this.post("解析錯誤")
              })
        }catch(err){
        }
    }
    _onLayout1(event){
        let {x, y, width, height} = event.nativeEvent.layout;
        this.setState({
            width1:width-30,
            height1:height
        },()=>{});
    }
    _onValueChange(itemValue,itemIndex){
        this.setState({dropdown:itemValue})
        this.devid=this.devids[itemValue].label
        this.post('itemValue:'+itemValue)
        this.post('itemIndex:'+itemIndex)
        this.post('devid:'+this.devid)
    }
    dispair(){
        if(this.devid=='尚未配對任何機型'){
            Smart.Show('目前沒有任何已配對音箱')
        }else{
            try{
                let body = JSON.stringify({})
                RNFetchBlob.config({
                }).fetch('DELETE','https://dimension8ai.com/auth/api/accountLinks/me/mac-address/'+this.devid,
                {'Content-Type': 'application/json','Authorization':'Bearer '+this.token},body)
                .then((res)=>{
                    this.post(res.text())
                    if(res.json().success){
                        this.post('解除成功')
                        Smart.Show(this.devid+' 已解除，請重新選擇型號!')
                        this.report_dispair(this.devid)
                    }else{
                        this.post('解除失敗')
                    }
                } ).catch((err) => {
                    this.post("解析錯誤")
                  })
            }catch(err){
            }
        }
    }
    report_dispair=async(devid)=>{
        var time=DateFormats(new Date())
        try{
            let body = JSON.stringify({
                "act": "upDevId",
                "fields": {
                    "devId": devid
                },
                "newValues": {
                    "devId": devid,
                    "logoutTime": time
                }
            })
            RNFetchBlob.config({
            }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
            .then((res)=>{
                this.post(res.text())
                this.deleteSip(devid)
            } ).catch((err) => {
                this.post("解析錯誤")
              })
        }catch(err){
        }
    }
    deleteSip=async(devid)=>{
        try{
            let body = JSON.stringify({
                "act": "deleteSip",
                "fields": {
                    "devId": devid
                }
            })
            RNFetchBlob.config({
            }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
            .then((res)=>{
                this.post(res.text())
                this.goback()
            } ).catch((err) => {
                this.post("解析錯誤")
              })
        }catch(err){
        }
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}>
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}}onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>解除綁定</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2',flexDirection:'row'}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:39}}></View>
                        <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>現有序號</Text>
                        <View style={{flex:12}}></View>
                        <View style={{height:44}}>
                            <View style={{flex:1,backgroundColor:"#ffffff",color:"#000000",borderRadius:22,borderColor:'#000000',alignItems:'center'}}onLayout={this._onLayout1}>
                                <Picker 
                                style={{width:this.state.width1,alignItems:'center'}}
                                selectedValue={this.state.dropdown}
                                mode='dropdown'
                                onValueChange={(itemValue,itemIndex)=>this._onValueChange(itemValue,itemIndex)}>
                                {this.devids.map((o,index)=> <Picker.Item key={o.value} label={o.label}value={o.value}/>)}    
                                </Picker>
                            </View>
                        </View>
                        <View style={{flex:29}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity style={styles.button1}onPress={this.dispair}>
                                <Text style={styles.buttontext1}>解除</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:56}}></View>
                        <View style={{flex:24}}></View>
                        <View style={{flex:18}}></View>
                        <View style={{flex:44}}></View>
                        <View style={{flex:262}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button1:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
      buttontext1:{
        flex:1,
        fontSize:14,
        textAlign:"center",
        textAlignVertical: 'center',
        color:"#ffffff"
    },
});