import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler,
    ScrollView
} from 'react-native';
const {Smart}=NativeModules;
export default class Setting_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this._onLayout2 = this._onLayout2.bind(this)
        this.state={
        }

    }
    componentDidMount() {
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    goback=async()=>{
        this.props.navigation.render('Main')
    }
    _onLayout2(event){
        let {x, y, width, height} = event.nativeEvent.layout;
        this.setState({
            width2:width,
            height2:height
        });
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}> 
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}} onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>設置</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:498,backgroundColor:'#f2f2f2'}}>
                    <ScrollView>
                        <View style={{height:56,flexDirection:'row'}}>
                            <Text style={{flex:72,fontSize:32,textAlign:'center',textAlignVertical:'center'}}>圖</Text>
                            <TouchableOpacity style={{flex:303}}>
                                <Text style={{flex:1,fontSize:16,textAlignVertical:'center'}}>Devid改名</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:1,flexDirection:'row'}}>
                            <View style={{flex:72}}></View>
                            <View style={{flex:303,backgroundColor:'#000000'}}></View>
                        </View>
                        <View style={{height:56,flexDirection:'row'}}>
                            <Text style={{flex:72,fontSize:32,textAlign:'center',textAlignVertical:'center'}}>圖</Text>
                            <TouchableOpacity style={{flex:303}}onPress={()=>{this.props.navigation.navigate('Sip')}}>
                                <Text style={{flex:1,fontSize:16,textAlignVertical:'center'}}>電話簿</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:1,flexDirection:'row'}}>
                            <View style={{flex:72}}></View>
                            <View style={{flex:303,backgroundColor:'#000000'}}></View>
                        </View>
                        <View style={{height:56,flexDirection:'row'}}>
                            <Text style={{flex:72,fontSize:32,textAlign:'center',textAlignVertical:'center'}}>圖</Text>
                            <TouchableOpacity style={{flex:303}}onPress={()=>{this.props.navigation.navigate('Dispair')}}>
                                <Text style={{flex:1,fontSize:16,textAlignVertical:'center'}}>解除綁定</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:1,flexDirection:'row'}}>
                            <View style={{flex:72}}></View>
                            <View style={{flex:303,backgroundColor:'#000000'}}></View>
                        </View>
                        <View style={{height:56,flexDirection:'row'}}>
                            <Text style={{flex:72,fontSize:32,textAlign:'center',textAlignVertical:'center'}}>圖</Text>
                            <TouchableOpacity style={{flex:303}}onPress={()=>{this.props.navigation.navigate('Pair')}}>
                                <Text style={{flex:1,fontSize:16,textAlignVertical:'center'}}>綁定機型</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:1,flexDirection:'row'}}>
                            <View style={{flex:72}}></View>
                            <View style={{flex:303,backgroundColor:'#000000'}}></View>
                        </View>
                        <View style={{height:56,flexDirection:'row'}}>
                            <Text style={{flex:72,fontSize:32,textAlign:'center',textAlignVertical:'center'}}>圖</Text>
                            <TouchableOpacity style={{flex:303}}onPress={()=>{this.props.navigation.navigate('Connect')}}>
                                <Text style={{flex:1,fontSize:16,textAlignVertical:'center'}}>音箱配網</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:1,flexDirection:'row'}}>
                            <View style={{flex:72}}></View>
                            <View style={{flex:303,backgroundColor:'#000000'}}></View>
                        </View>
                        <View style={{height:56,flexDirection:'row'}}>
                            <Text style={{flex:72,fontSize:32,textAlign:'center',textAlignVertical:'center'}}>圖</Text>
                            <TouchableOpacity style={{flex:303}} onPress={()=>{this.props.navigation.navigate('Login')}}>
                                <Text style={{flex:1,fontSize:16,textAlignVertical:'center'}}>登出</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:1,flexDirection:'row'}}>
                            <View style={{flex:72}}></View>
                            <View style={{flex:303,backgroundColor:'#000000'}}></View>
                        </View>
                    </ScrollView>
                </View>
                <View style={{flex:89,flexDirection:'row'}}>
                    <TouchableOpacity style={{flex:1}}onLayout={this._onLayout2}onPress={()=>{this.props.navigation.replace('Main')}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item1.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}onPress={()=>{this.props.navigation.replace('Dialoge')}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item2.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item3.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item4.png')}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
