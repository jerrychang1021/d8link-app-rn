import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    NativeModules,
    TouchableOpacity,
    Image,
    Picker,
    Platform,
    BackHandler
} from 'react-native';
const {Smart}=NativeModules;
import RNFetchBlob from 'rn-fetch-blob'
import AsyncStorage from '@react-native-community/async-storage';
export default class Choose_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    devids=[]
    constructor (props){
        super(props)
        this._onLayout1 = this._onLayout1.bind(this)  
        this._onValueChange = this._onValueChange.bind(this)     
        this.gotopair = this.gotopair.bind(this)
        this.gotomain = this.gotomain.bind(this)
        this.state={
            width1:1,
            height1:1,
            dropdown:'',
            devids:''
        }
        this.getdevid()
    }
    btn_press=false
    componentDidMount(){
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    goback=async()=>{
        this.props.navigation.navigate('Login')
    }
    _OnPress_1=async()=>{
        console.log(await AsyncStorage.getItem('token'))
    }
    getdevid=async()=>{
        this.token=await AsyncStorage.getItem('token')
        try{
            let body = JSON.stringify({})
            RNFetchBlob.config({
            }).fetch('GET','https://dimension8ai.com/auth/api/accountLinks/me/mac-address',
            {'Content-Type': 'application/json','Authorization':'Bearer '+this.token},body)
            .then((res)=>{
                console.log(res.text())
                if(Object.keys(res.json()).length==0) this.devids.push({label: '尚未配對任何機型',value:0})

                else for(i=0;i<Object.keys(res.json()).length;i++){
                    this.devids.push({label: res.json()[i].provider_user_id,value:i})
                }
                this.setState({
                    devids:''
                },()=>{});
            } ).catch((err) => {
                console.log("解析錯誤")
              })
        }catch(err){
        }
    }
    _onLayout1(event){
        let {x, y, width, height} = event.nativeEvent.layout;
        this.setState({
            width1:width-30,
            height1:height
        },()=>{});
    }
    _onValueChange(itemValue,itemIndex){
        this.setState({dropdown:itemValue})
        console.log('itemValue:'+itemValue)
        console.log('itemIndex:'+itemIndex)
    }
    gotomain(){
        this.props.navigation.navigate('Main')
    }
    gotopair(){
        this.props.navigation.navigate('Pair')
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}>
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}}onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>Link綁定</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2',flexDirection:'row'}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:39}}></View>
                        <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>現有序號</Text>
                        <View style={{flex:12}}></View>
                        <View style={{height:44}}>
                            <View style={{flex:1,backgroundColor:"#ffffff",color:"#000000",borderRadius:22,borderColor:'#000000',alignItems:'center'}}onLayout={this._onLayout1}>
                                <Picker 
                                style={{width:this.state.width1,alignItems:'center'}}
                                selectedValue={this.state.dropdown}
                                mode='dropdown'
                                onValueChange={(itemValue,itemIndex)=>this._onValueChange(itemValue,itemIndex)}>
                                {this.devids.map((o,index)=> <Picker.Item key={o.value} label={o.label}value={o.value}/>)}    
                                </Picker>
                            </View>
                        </View>
                        <View style={{flex:29}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity style={styles.button1}onPress={this.gotomain}>
                                <Text style={styles.buttontext1}>以現有帳號繼續</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:56}}></View>
                        <View style={{height:24}}>
                            <Text style={{flex:1,color:'#000000',fontSize:16,textAlign:'center'}}>或尚未有序號嗎?</Text>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity style={styles.button1} onPress={this.gotopair}>
                                <Text style={styles.buttontext1}>進行新配對</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:262}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    button1:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
      buttontext1:{
        flex:1,
        fontSize:14,
        textAlign:"center",
        textAlignVertical: 'center',
        color:"#ffffff"
    },
});