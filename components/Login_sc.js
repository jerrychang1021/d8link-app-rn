import React from 'react'
import{
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
const {Smart,Volley1}=NativeModules;
import RNFetchBlob from 'rn-fetch-blob'
export default class Login_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this._OnPress_1 = this._OnPress_1.bind(this)    
        this.gotoregister = this.gotoregister.bind(this)
        this.state={
            text_act:'',
            text_psd:'',
        }
        this._getData()
    }
    componentDidMount(){
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                return false;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    post(message){
        Smart.Log(message)
        console.log(message)
    }
    _OnPress_1(){
        this.post("帳號:"+this.state.text_act)
        this.post("密碼:"+this.state.text_psd)
        if (this.state.text_act==""){
            Smart.Show('帳號欄不可為空')
        }
        else if(this.state.text_psd==""){
            Smart.Show('密碼欄不可為空')
        }else{
            try{
                let body = JSON.stringify({
                    auth_type:'basic',
                    username:this.state.text_act,
                    password:this.state.text_psd,
                })
                RNFetchBlob.config({
                }).fetch('POST','https://dimension8ai.com/auth/api/sessions',{'Content-Type': 'application/json'},body)
                .then((res)=>{
                    this.post(res.text())
                    if(res.json().error == null){
                        this.userid=res.json().user_id
                        this.username=res.json().user_name
                        this.token=res.json().token
                        this.post("登入成功")
                        this.post("user_id = "+this.userid)
                        this.post("user_name = "+this.username)
                        this._storeData(this.state.text_act,this.state.text_psd,this.userid,this.username,this.token)               
                    }
                    else {
                        Smart.Show('登入失敗，請確認帳密')
                        this.post("登入失敗，帳密不正確")
                    }
                } ).catch((err) => {
                    this.post("解析錯誤")
                  })
            }catch(err){

            }
        }
    }
    _onLayout1(event){
        let {x, y, width, height} = event.nativeEvent.layout;
        this.setState({
            size1:height-4
        });
    }
    gotochoose(){
        this.props.navigation.navigate('Choose')
    }
    gotoregister(){
        this.props.navigation.replace('Register')
    }
    _getData=async()=>{
        this.post('嘗試獲取帳號密碼')
        try {
            const account = await AsyncStorage.getItem('account');
            const password = await AsyncStorage.getItem('password');
            if (account !== null) {
                this.post('獲取成功:'+'\n'+account+'\n'+password)
                this.setState({
                    text_act:account,
                    text_psd:password
                });
            }
        } catch (error) {
              console.log("無帳號密碼存檔")
        }
    }
    _storeData = async (account,password,userid,username,token) => {
        try{
            await AsyncStorage.setItem('account',account);
            await AsyncStorage.setItem('password',password);
            await AsyncStorage.setItem('userid',userid);
            await AsyncStorage.setItem('username',username);
            await AsyncStorage.setItem('token',token);
        }catch (error) {
            this.post('登入儲存錯誤')
        }
        this.gotochoose()
    }
    render(){
        return(
            <View style={{flex: 1}}>
                <View style={{flex:174,backgroundColor:"#ff6800"}}>
                    <View style={{flex:24,flexDirection:"row"}}>
                        <View style={{flex:322}}></View>
                        <View style={{width:46}}>
                            <View style={{height:7}}></View>
                            <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                        </View>
                        <View style={{flex:7}}></View>
                    </View>
                    <View style={{flex:88}}></View>
                    <View style={{height:40,flexDirection: 'row'}}>
                        <View style={{flex:53}}></View>
                        <View style={{flex:322}}>
                            <Text style={{height:40,color:"#ffffff",fontSize:34}}>{"d’Octo Link"}</Text>
                        </View>
                    </View>
                    <View style={{flex:22}}></View>
                </View>
                <View style={{flex:493,flexDirection: 'row',backgroundColor:"#f2f2f2"}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:33}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>帳號</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(text_act)=>this.setState({text_act})}
                            placeholder='請輸入'>{this.state.text_act}</TextInput>
                        </View>
                        <View style={{flex:25}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>密碼</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(text_psd)=>this.setState({text_psd})}
                            placeholder='請輸入'
                            secureTextEntry={true}>{this.state.text_psd}</TextInput>
                        </View>
                        <View style={{height:9}}></View>
                        <View style={{flex:22}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity style={styles.button1}onPress={this._OnPress_1}>
                                <Text style={styles.buttontext1}>登入</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:32}}></View>
                        <View style={{height:24,flexDirection:'row'}}>
                            <View style={{flex:1}}></View>
                            <Text style={{fontSize:16,textAlign:'right'}}>尚未註冊嗎?點此</Text>
                            <Text style={{fontSize:16,textAlign:'left',color:'#4a90e2'}}onPress={this.gotoregister}>註冊</Text>
                            <View style={{flex:1}}></View>
                        </View>
                        <View style={{flex:74}}></View>
                        <View style={{height:77.5,flexDirection: 'row'}}>
                            <View style={{flex:1}}></View>
                            <Image style={{width:130,height:77.5,}}source={require('./asset_image/login_item2.png')}></Image>
                            <View style={{flex:1}}></View>
                        </View>
                        <View style={{flex:20.5}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View> 
        )
    }
}
const styles = StyleSheet.create({
    button1:{
      flex:1,
      backgroundColor:"#ff6800",  
      color:"#ffffff",
      fontSize:14,
      borderRadius:22,
    },
    buttontext1:{
      flex:1,
      fontSize:14,
      textAlign:"center",
      textAlignVertical: 'center',
      color:"#ffffff"
    },
    textInput1:{
        flex:1,
        backgroundColor:"#ffffff",
        paddingLeft:15,
        textAlignVertical:'center',
        borderRadius:22,
        borderColor:"#000000",
        borderWidth:1,
        fontSize:16,
    }
});
