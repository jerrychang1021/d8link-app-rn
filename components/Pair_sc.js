import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler
} from 'react-native';
const {Smart}=NativeModules;
import RNFetchBlob from 'rn-fetch-blob'
import AsyncStorage from '@react-native-community/async-storage';
import { Thread } from 'react-native-threads';
export default class Pair_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this.gotomain = this.gotomain.bind(this)        
        this.state={
            width1:1,
            height1:1,
            wifi:'grkdegjkropkg',
            password:'drjuiogdrkg',
            btn_color:'#ff6800',
            btn_text:'開始配對'
        }
    }

    componentDidMount(){
        this.Pairthread = new Thread('././Pairthread.js')
        this.Pairthread.onmessage = this.handleMessage;
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.Pairthread.terminate();
        this.Pairthread = null;
        this.backHandler.remove();
    }
    goback=async()=>{
        Smart.StopConfig()
        this.Pairthread.postMessage('Stop')
        this.props.navigation.navigate('Choose')
    }
    post(message){
        Smart.Log(message)
        console.log(message)
    }
    handleMessage = async(message) => {
        this.post('Pairthread 回傳:\n'+message)
        if(message=='配對完成'){
            Smart.Show('配對完成')
            this.setState({
                btn_color:'#ff6800',
                btn_text:'開始配對'
            });
            Smart.StopConfig()
            this.btn_press=!this.btn_press
            this.Pairthread.postMessage('Stop')
            this.gotomain()
        }
    }
    getlintoken=async()=>{
        this.token=await AsyncStorage.getItem('token')
        this.userid=await AsyncStorage.getItem('userid')
        try{
            let body = JSON.stringify({})
            RNFetchBlob.config({
            }).fetch('POST','https://dimension8ai.com/auth/api/users/'+this.userid+'/linkTokens',
            {'Content-Type': 'application/json','Authorization':'Bearer '+this.token},body)
            .then((res)=>{
                this.linktoken=res.json().linkToken
                console.log(this.linktoken)
                Smart.SmartConfig(this.state.wifi,this.state.password + "\n" + this.userid + "\n" + this.linktoken)
            }).catch((err) => {
                console.log("解析錯誤")
              })
        }catch(err){
        }
    }
    dopair=async()=>{
        if(this.state.wifi=='')Smart.Show('WIFI欄位不可為空')
        else if(this.state.password=='')Smart.Show('密碼欄位不可為空')
        else if(this.state.password.length<8)Smart.Show('密碼字數須為8位以上')
        else if(this.btn_press){
            this.setState({
                btn_color:'#ff6800',
                btn_text:'開始配對'
            });
            Smart.StopConfig()
            this.btn_press=!this.btn_press
            this.Pairthread.postMessage('Stop')
        }else{
            this.getlintoken()
            this.setState({
                btn_color:'#ff0000',
                btn_text:' 停止配對'
            },()=>{});
            this.btn_press=!this.btn_press
            this.Pairthread.postMessage('Start')
        }
    }
    gotomain(){
        this.props.navigation.replace('Main')
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}>
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}}onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>配對音箱</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2',flexDirection:'row'}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:39}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>wifi</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(wifi)=>this.setState({wifi})}
                            placeholder='請輸入'></TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>密碼</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(password)=>this.setState({password})}
                            placeholder='請輸入'
                            secureTextEntry={true}></TextInput>
                        </View>
                        <View style={{flex:31}}></View>
                        <View style={{height:44}}>
                            <TouchableOpacity
                            style={{
                                flex:1,
                                backgroundColor:this.state.btn_color,
                                color:"#ffffff",
                                fontSize:14,
                                borderRadius:22,}}
                            onPress={this.dopair}>
                                <Text style={styles.buttontext1}>{this.state.btn_text}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex:311}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button1:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
    buttontext1:{
        flex:1,
        fontSize:14,
        textAlign:"center",
        textAlignVertical: 'center',
        color:"#ffffff"
    },
    textInput1:{
        flex:1,
        backgroundColor:"#ffffff",
        paddingLeft:15,
        textAlignVertical:'center',
        borderRadius:22,
        borderColor:"#000000",
        borderWidth:1,
        fontSize:16,
    }
});