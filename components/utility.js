function DateFormats(r){
    year=(r.getYear()+1900).toString()
    month=r.getMonth()+1
    if (month<10) month='0'+month.toString()
    else month = month.toString()
    date = r.getDate()
    if (date<10) date='0'+date.toString()
    else date = date.toString()
    hour=r.getHours()
    if (hour<10) hour='0'+hour.toString()
    else hour = hour.toString()
    min=r.getMinutes()
    if (min<10) min='0'+min.toString()
    else min = min.toString()
    sec=r.getSeconds()
    if (sec<10) sec='0'+sec.toString()
    else sec = sec.toString()
    r = year+'-'+month+'-'+date+' '+hour+':'+min+':'+sec
    return r
}
export {DateFormats};