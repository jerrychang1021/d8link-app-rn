import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Picker,
    Platform,
    BackHandler,
    ScrollView
} from 'react-native';
const {Smart}=NativeModules;
import RNFetchBlob from 'rn-fetch-blob'
import AsyncStorage from '@react-native-community/async-storage';
import { Thread } from 'react-native-threads';
export default class Dialogue_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this._onLayout1 = this._onLayout1.bind(this)
        this._onLayout2 = this._onLayout2.bind(this)
        this.state={
            time:["04:50PM"],
            ans:["你可以再問一次嗎？你可以再問一次嗎？你"],
            logweigth:280,
        }
    }
    componentDidMount(){
        this.Pairthread = new Thread('././Pairthread.js')
        this.Pairthread.onmessage = this.handleMessage;
        this.Pairthread.postMessage('Start_Getlogue')
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.Pairthread.terminate();
        this.Pairthread = null;
        this.backHandler.remove();
    }
    goback=async()=>{
        this.Pairthread.postMessage('Stop_Getlogue')
        this.props.navigation.navigate('Main')
    }
    post(message){
        Smart.Log(message)
        console.log(message)
    }
    handleMessage = async(message) => {
        this.post('Pairthread 回傳:\n'+message)
    }
    _onLayout1(event){
        let {x, y, width, height} = event.nativeEvent.layout;
        this.setState({
            width2:width,
            height2:height,
        });
    }
    _onLayout2(event){
        let {x, y, width, height} = event.nativeEvent.layout;
        this.setState({
            logweigth:width-86,
        });        
        /*console.log(this.toleigth(this.state.ans[0]))
        if(this.toleigth(this.state.ans[0])/40>1){
            this.setState({
                a:16*Math.floor(this.toleigth(this.state.ans[0])/40),
            });
        }*/
    }
    toleigth(message){
        var leigth=0
        for(i=0;i<message.length;i++){
            if(/.*[\u4e00-\u9fa5]+.*$/.test(message[i])) {//中文字
                leigth = leigth + 2 
            }
            else if(/.*[A-Za-z]+.*$/.test(message[i])) {//英文字
                leigth = leigth + 1 
            }
            else if(/.*[0-9]+.*$/.test(message[i])) {//數字
                leigth = leigth + 1 
            }
            else leigth = leigth + 2
        }
        return leigth
    }
    render(){
        return(
            <View style={{flex:1}}onLayout={this._onLayout2}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}> 
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}} onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>對話內容</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:498,backgroundColor:'#f2f2f2'}}>
                    <ScrollView>
                        <View style={{height:64+16*Math.floor(this.toleigth(this.state.ans[0])/Math.floor(this.state.logweigth/7)),flexDirection:'row'}}>
                            <View style={{width:16}}></View>
                            <View style={{width:40}}>
                                <View style={{height:16}}></View>
                                <Image style={{height:40,width:40}}source={require('./asset_image/ying_item1.png')}></Image>
                            </View>
                            <View style={{width:10}}></View>
                            <View style={{flex:1,paddingRight:10}}>
                                <View style={{height:14}}></View>
                                <Text style={{height:22,fontSize:16,paddingLeft:5}}>D8Link {this.state.time[0]}</Text>
                                <Text style={{height:24+16*Math.floor(this.toleigth(this.state.ans[0])/Math.floor(this.state.logweigth/7)),
                                fontSize:14,borderRadius:12,backgroundColor:'#ffffff',paddingLeft:5,paddingRight:5,paddingTop:2}}
                                >{this.state.ans[0]}</Text>
                                <View style={{height:4}}></View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={{flex:89,flexDirection:'row'}}>
                    <TouchableOpacity style={{flex:1}}onLayout={this._onLayout1}onPress={()=>{this.props.navigation.navigate('Main')}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item1.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item2.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item3.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1}}onPress={()=>{this.props.navigation.replace('Setting')}}>
                        <Image style={{width:this.state.width2,height:this.state.height2}}source={require('./asset_image/main_item4.png')}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
