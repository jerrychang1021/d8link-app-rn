import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler,
    ScrollView
} from 'react-native';
const {Smart}=NativeModules;
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob'
import {DateFormats} from './utility'
export default class Sip_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this._add = this._add.bind(this)    
        this.state={
            names:[],
            nicks:[],
            phones:[]
        }
        this.querySip()
    }
    componentDidMount() {
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    goback=async()=>{
        this.props.navigation.replace('Setting')
    }
    post(message){
        Smart.Log(message)
        console.log(message)
    }
    _edit=async(i)=>{
        this.post('edit:')
        this.post(this.state.names[i])
        this.post(this.state.nicks[i])
        this.post(this.state.phones[i])
        try{
            var time = DateFormats(new Date())
            let body = JSON.stringify({
                "act": "upSip",
                "fields": {
                    "devId": this.devid
                },
                "newValues": {
                    "updateTime": time,
                    "contacts": []
                }            
            })
            body=JSON.parse(body)
            for(x=0;x<this.state.names.length;x++){
                if (x!=i) body.newValues.contacts.push({phone : this.state.phones[x],name : this.state.names[x],nicks:[this.state.nicks[x]]})
            }
            body=JSON.stringify(body)
            this.post(body)
            try{
                await AsyncStorage.setItem('Sipbody',body);
                await AsyncStorage.setItem('SipNmae',this.state.names[i]);
                await AsyncStorage.setItem('SipNick',this.state.nicks[i]);
                await AsyncStorage.setItem('SipPhone',this.state.phones[i]);
                this.props.navigation.replace('Sipedit')
            }catch (error) {
                this.post('儲存錯誤')
            }
        }catch(err){

        }
    }
    _add(){
        var time = DateFormats(new Date())
        try{
            let body = JSON.stringify({
                "act": "upSip",
                "fields": {
                    "devId": this.devid
                },
                "newValues": {
                    "updateTime": time,
                    "contacts": []
                }            
            })
            body=JSON.parse(body)
            for(i=0;i<this.state.names.length;i++){
                body.newValues.contacts.push({phone : this.state.phones[i],name : this.state.names[i],nicks:[this.state.nicks[i]]})
            }
            body.newValues.contacts.push({phone:'0',name:'new name'+this.state.names.length.toString(),nicks:['new nick'+this.state.names.length.toString()]})
            body=JSON.stringify(body)
            RNFetchBlob.config({
            }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
            .then((res)=>{
                this.post(res.text())
                this.querySip()
            } ).catch((err) => {
                this.post("解析錯誤")
              })
        }catch(error){

        }
    }
    querySip=async()=>{
        this.devid = await AsyncStorage.getItem('devid')
        this.post(this.devid)
        try{
            let body = JSON.stringify({
                act: "querySip",
                fields: {
                    devId: this.devid
                }
            })
            RNFetchBlob.config({
            }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
            .then((res)=>{
                this.post(res.text())
                if(res.json().msg=='devId not Found') this.post('未搜尋到Sip')
                else{
                    let contacts = JSON.parse(res.json().data[0].contacts)
                    this.setState({
                        names: [],
                        nicks: [],
                        phones: []
                    });
                    for(i=0;i<Object.keys(contacts).length;i++){
                        this.setState({
                            names: [...this.state.names , contacts[i].name],
                            nicks: [...this.state.nicks , contacts[i].nicks[0]],
                            phones: [...this.state.phones , contacts[i].phone]
                        },()=>{if(i==Object.keys(contacts).length-1) this.post(JSON.stringify(this.state))});
                    }
                }
            } ).catch((err) => {
                this.post("解析錯誤")
              })
        }catch(err){

        }
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}> 
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}} onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>電話簿</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2'}}>
                    <ScrollView>
                        {this.state.nicks.map((message, i) => 
                            <View style={{height:70}} key={i}>
                                <View style={{height:70,flexDirection:'row'}}>
                                    <TouchableOpacity style={{flex:1,flexDirection:'row'}}>
                                        <Image style={{width:70,height:70}}source={require('./asset_image/people_item1.png')}></Image>
                                        <Text style={{flex:1,fontSize:24,textAlignVertical:'center'}}>{message}</Text>
                                    </TouchableOpacity>
                                    <View style={{width:30,justifyContent:'center'}}>
                                        <TouchableOpacity style={{width:30,height:30,justifyContent:'center',borderRadius:15,borderWidth:1,borderColor:'#0000FF'}}
                                        onPress={()=>{this._edit(i)}}>
                                            <Text style={{flex:1,fontSize:25,textAlignVertical:'center',textAlign:'center',color:'#0000FF'}}>!</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{width:5}}></View>
                                </View>
                                <View style={{height:1,flexDirection:'row'}}>
                                    <View style={{width:70}}></View>
                                    <View style={{flex:1,backgroundColor:'#000000'}}></View>
                                </View>
                            </View>
                        )}
                    </ScrollView>
                    <TouchableOpacity style={{   
                        position:'absolute',
                        backgroundColor:"#00BBFF",
                        borderRadius:25,
                        width:50,
                        height:50,
                        bottom:30,
                        right:30
                    }}
                    onPress={this._add}>
                        <Text style={{
                            flex:1,
                            fontSize:25,
                            textAlign:"center",
                            textAlignVertical: 'center',
                            color:"#ffffff"
                        }}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
