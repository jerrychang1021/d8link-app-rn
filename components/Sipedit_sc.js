import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    NativeModules,
    TouchableOpacity,
    Image,
    Platform,
    BackHandler,
    ScrollView
} from 'react-native';
const {Smart}=NativeModules;
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob'
import {DateFormats} from './utility'
export default class Sipedit_sc extends React.Component{
    static navigationOptions={
        header:null,
    }
    constructor (props){
        super(props)
        this._delete = this._delete.bind(this)    
        this._edit = this._edit.bind(this)    
        this.state={
        }
        this.getstore()
    }
    componentDidMount() {
        if(Platform.OS === "android") {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.goback();
                return true;
            });
        }
    }
    componentWillUnmount(){
        this.backHandler.remove();
    }
    goback=async()=>{
        this.props.navigation.replace('Sip')
    }
    getstore=async()=>{
        try {
            this.sipbody=await AsyncStorage.getItem('Sipbody');
            this.name=await AsyncStorage.getItem('SipNmae');
            this.nick=await AsyncStorage.getItem('SipNick');
            this.phone=await AsyncStorage.getItem('SipPhone');
            this.setState({
                name: this.name,
                nick: this.nick,
                phone: this.phone
            });
        }catch (error){

        }
    }
    _delete(){
        try{
            let body = this.sipbody
            RNFetchBlob.config({
            }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
            .then((res)=>{
                console.log(res.text())
                this.goback()
            } ).catch((err) => {
                console.log("解析錯誤")
              })
        }catch(err){

        }
    }
    _edit(){
        try{
            let body = JSON.parse(this.sipbody)
            body.newValues.contacts.push({phone : this.state.phone,name : this.state.name,nicks:[this.state.nick]})
            body=JSON.stringify(body)
            RNFetchBlob.config({
            }).fetch('POST','http://dimension8ai.com:6379/d8link',{'Content-Type': 'application/json'},body)
            .then((res)=>{
                console.log(res.text())
                this.goback()
            } ).catch((err) => {
                console.log("解析錯誤")
              })
        }catch(err){

        }
    }
    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:80,backgroundColor:'#ff6900'}}> 
                    <View style={{flex:7}}></View>
                    <View style={{height:10,flexDirection:'row'}}>
                        <View style={{flex:322}}></View>
                        <View style={{flex:46}}>
                            <View style={{flex:1}}>
                                <Image style={{width:46,height:10}}source={require('./asset_image/login_item1.png')}></Image>
                            </View>
                        </View>
                        <View style={{flex:10}}></View>
                    </View>
                    <View style={{flex:7}}></View>
                    <View style={{flex:15}}></View>
                    <View style={{height:24,flexDirection:'row'}}>
                        <View style={{flex:16}}></View>
                            <TouchableOpacity style={{width:24}} onPress={this.goback}>
                                <Image style={{width:24,height:24}}source={require('./asset_image/cross_item1.png')}></Image>
                            </TouchableOpacity>                        
                        <View style={{flex:32}}></View>
                        <Text style={{flex:303,fontSize:20,color:'#ffffff'}}>編輯聯絡人</Text> 
                    </View>
                    <View style={{flex:17}}></View>
                </View>
                <View style={{flex:587,backgroundColor:'#f2f2f2',flexDirection:'row'}}>
                    <View style={{flex:44}}></View>
                    <View style={{flex:287}}>
                        <View style={{flex:39}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>姓名</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(name)=>this.setState({name})}
                            placeholder='請輸入'>{this.state.name}</TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>稱呼</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(nick)=>this.setState({nick})}
                            placeholder='此欄位會顯示在電話簿'>{this.state.nick}</TextInput>
                        </View>
                        <View style={{flex:18}}></View>
                        <View style={{height:25}}>
                            <Text style={{height:25,color:"#ff6800",paddingLeft:12,fontSize:16}}>電話/手機</Text>
                        </View>
                        <View style={{flex:12}}></View>
                        <View style={{height:44,borderRadius:22,borderColor:"#000000",borderWidth:1}}>
                            <TextInput 
                            style={styles.textInput1}
                            onChangeText={(phone)=>this.setState({phone})}
                            placeholder='請輸入'>{this.state.phone}</TextInput>
                        </View>
                        <View style={{flex:30}}></View>
                        <View style={{height:44,flexDirection:'row'}}>
                            <View style={{flex:9}}>
                                <TouchableOpacity style={styles.button2} onPress={this._delete}>
                                    <Text style={styles.buttontext1}>刪除聯絡人</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1}}></View>
                            <View style={{flex:18}}>
                                <TouchableOpacity style={styles.button1} onPress={this._edit}>
                                    <Text style={styles.buttontext1}>編輯完成</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flex:249}}></View>
                    </View>
                    <View style={{flex:44}}></View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button1:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
    button2:{
        flex:1,
        backgroundColor:"#ff6800",  
        color:"#ffffff",
        fontSize:14,
        borderRadius:22,
    },
    buttontext1:{
        flex:1,
        fontSize:14,
        textAlign:"center",
        textAlignVertical: 'center',
        color:"#ffffff"
    },
    textInput1:{
        flex:1,
        backgroundColor:"#ffffff",
        paddingLeft:15,
        textAlignVertical:'center',
        borderRadius:22,
        borderColor:"#000000",
        borderWidth:1,
        fontSize:16,
    }
});
